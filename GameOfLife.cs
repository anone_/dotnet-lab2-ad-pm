﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace dotnet_lab2_AD_PM
{
    class GameOfLife
    {
        public readonly static uint map_size = 10;
        public bool[,] map;
        public readonly char dead = '-';
        public readonly char live = '*';

        private void _init_() {
            map = new bool[map_size,map_size];
            for (int i = 0; i < map_size; i++)
            {
                for (int j = 0; j < map_size; j++)
                {
                    map[i, j] = false;
                }
            }
        }

        private void generateRandom()
        {
            Random rand = new Random();
            for (int i = 0; i < map_size; i++)
            {
                for (int j = 0; j < map_size; j++)
                {
                    if (rand.Next(2) == 1)
                        map[i, j] = true;
                    else
                        map[i, j] = false;
                }
            }
        }

        /*liczba sasiadow number of neighbours*/
        private int non(int x, int y)
        {
            int count = 0;
            for (int i = (y-1); i <= (y+1); i++)
            {
                for (int j = (x-1); j <= (x+1); j++)
                {
                    if ((i >= 0 && i < map_size) && (j >= 0 && j < map_size) &&
                        map[i,j] && (i != x || j != y))
                            count++;
                }
            }

            return count;
        }

        private void displaySymbol(bool status)
        {
            if (status)
                Console.Write(live);
            else
                Console.Write(dead);
        }

        private void displayMap()
        {
            for (int i = 0; i < map_size; i++)
            {
                for (int j = 0; j < map_size; j++)
                {
                    displaySymbol(map[i,j]);
                }
                Console.Write("\n");
            }
        }

        private void play()
        {
            int count;
            for (int i = 0; i < map_size; i++)
            {
                for (int j = 0; j < map_size; j++)
                {
                    count = non(i, j);
                    if(map[i,j])
                    {
                        if (count < 2)
                            map[i, j] = false;
                        else if (count > 3)
                            map[i, j] = false;
                    }
                    else
                    {
                        if (count == 3)
                            map[i, j] = true;
                    }

                }
            }
        }

        GameOfLife()
        {
            _init_();
        }

        static void Main(string[] args)
        {
            GameOfLife mygame = new GameOfLife();
            mygame.generateRandom();
            while(true)
            {
                mygame.displayMap();
                mygame.play();
                Console.ReadKey();
                Console.Clear();
            }
        }
    }
}
